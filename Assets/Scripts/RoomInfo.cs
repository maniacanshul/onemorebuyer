﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomInfo : MonoBehaviour
{
    public List<Sprite> roomSprites;
    public List<Sprite> roomSpritesHighlighted;
    public CATEGORY_SPACE categorySpace;
    public CATEGORY_CONV categoryConv;
    public CATEGORY_FEATURES categoryFeatures;
    public CATEGORY_ARCH categoryArch;
    public CATEGORY_SOCIAL categorySocial;

    private RoomsManager roomsManager;

    public void Init(RoomsManager _roomsManager)
    {
        roomsManager = _roomsManager;

        categorySpace = (CATEGORY_SPACE)Random.Range(0, 2);
        categoryConv = (CATEGORY_CONV)Random.Range(0, 2);
        categoryFeatures = (CATEGORY_FEATURES)Random.Range(0, 2);
        categoryArch= (CATEGORY_ARCH)Random.Range(0, 2);
        categorySocial = (CATEGORY_SOCIAL)Random.Range(0, 2);
    }
}
