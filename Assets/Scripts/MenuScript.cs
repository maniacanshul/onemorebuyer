﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    public GameObject creditsPage;
    public GameObject instructions;

    public void PlayGame()
    {
        AudioManager.Instance.PlayThisClip(AUDIO_SFX.BUTTON_POP);
        SceneManager.LoadScene(1);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void ShowCredits()
    {
        creditsPage.SetActive(true);
        AudioManager.Instance.PlayThisClip(AUDIO_SFX.BUTTON_POP);
    }

    public void EnableInstructionButton()
    {
        instructions.SetActive(true);
        AudioManager.Instance.PlayThisClip(AUDIO_SFX.BUTTON_POP);
    }

    public void DisableInstructionButton()
    {
        instructions.SetActive(false);
        AudioManager.Instance.PlayThisClip(AUDIO_SFX.BUTTON_POP);
    }

    public void HideCredits()
    {
        creditsPage.SetActive(false);
        AudioManager.Instance.PlayThisClip(AUDIO_SFX.BUTTON_POP);
    }
}
