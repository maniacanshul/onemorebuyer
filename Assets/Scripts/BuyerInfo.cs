﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyerInfo : MonoBehaviour
{
    public CATEGORY_SPACE categorySpace;
    public CATEGORY_CONV categoryConv;
    public CATEGORY_FEATURES categoryFeatures;
    public CATEGORY_ARCH categoryArch;
    public CATEGORY_SOCIAL categorySocial;

    public string categorySpaceAnswer;
    public string categoryConvAnswer;
    public string categoryFeatAnswer;
    public string categoryArchAnswer;
    public string categorySocialAnswer;

    private BuyerManager buyerManager;

    public void InitBuyer(BuyerManager _buyerManager)
    {
        buyerManager = _buyerManager;
    }

    public void SetupCategoryEnums(CATEGORY_SPACE space, CATEGORY_CONV conv, CATEGORY_FEATURES feat, CATEGORY_ARCH arch, CATEGORY_SOCIAL social)
    {
        categorySpace = space;
        categoryConv = conv;
        categoryFeatures = feat;
        categoryArch = arch;
        categorySocial = social;
    }

    public void SetupBuyerQuestionsText(string space, string conv, string feat, string arch, string social)
    {
        categorySpaceAnswer = space;
        categoryConvAnswer = conv;
        categoryFeatAnswer = feat;
        categoryArchAnswer = arch;
        categorySocialAnswer = social;
    }
}
