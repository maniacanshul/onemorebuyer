﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyerManager : MonoBehaviour
{
    public GameObject buyer;

    public BuyerInfo currentActiveBuyer;

    private GameManager gameManager;

    public void Init(GameManager _gameManager)
    {
        gameManager = _gameManager;
    }

    public void SpawnBuyer(int space, int conv, int feat, int arch, int social)
    {
        GameObject tempBuyer = Instantiate(buyer) as GameObject;
        currentActiveBuyer = tempBuyer.GetComponent<BuyerInfo>();

        currentActiveBuyer.InitBuyer(this);

        int spaceSubCategory = Random.Range(0, 2);
        int featSubCategory = Random.Range(0, 2);
        int convSubCategory = Random.Range(0, 2);
        int archSubCategory = Random.Range(0, 2);
        int socialSubCategory = Random.Range(0, 2);

        currentActiveBuyer.SetupCategoryEnums((CATEGORY_SPACE)spaceSubCategory, (CATEGORY_CONV)featSubCategory, (CATEGORY_FEATURES)convSubCategory, (CATEGORY_ARCH)archSubCategory, (CATEGORY_SOCIAL)socialSubCategory);

        string spaceAnswer = gameManager.categoriesData.categorySpaceInfo.answersInfo[spaceSubCategory].answerOptions[space];
        string convAnswer = gameManager.categoriesData.categoryConvInfo.answersInfo[convSubCategory].answerOptions[conv];
        string featAnswer = gameManager.categoriesData.categoryFeatInfo.answersInfo[featSubCategory].answerOptions[feat];
        string archAnswer = gameManager.categoriesData.categoryArchInfo.answersInfo[archSubCategory].answerOptions[arch];
        string socialAnswer = gameManager.categoriesData.categorySocialInfo.answersInfo[socialSubCategory].answerOptions[social];

        currentActiveBuyer.SetupBuyerQuestionsText(spaceAnswer, convAnswer, featAnswer, archAnswer, socialAnswer);
    }
}
