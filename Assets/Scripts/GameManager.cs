﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public CategoriesData categoriesData;
    public SellerInfo sellerInfo;
    public BuyerManager buyerManager;
    public RoomsManager roomManager;
    public UIManager uiManager;

    [HideInInspector]
    public GAME_STATE gameState;

    private int questionSetCounter;

    public int playerScore;
    public int playerTotalScore;

    public int maxCommision;

    private int houseSold;

    void Start()
    {
        sellerInfo.Init(this);
        buyerManager.Init(this);
        roomManager.Init(this);
        uiManager.Init(this);

        houseSold = 0;
        playerScore = 0;
        playerTotalScore = 0;
        questionSetCounter = 0;

        gameState = GAME_STATE.CONVERSATION;
        uiManager.UpdateGameState(0);

        SetupNewBuyer();
    }

    public void SetupQuestion()
    {
        string questionText = "";
        switch (questionSetCounter)
        {
            case 0:
                {
                    questionText = sellerInfo.categorySpaceQuestion;
                    break;
                }
            case 1:
                {
                    questionText = sellerInfo.categoryConvQuestion;
                    break;
                }
            case 2:
                {
                    questionText = sellerInfo.categoryFeatQuestion;
                    break;
                }
            case 3:
                {
                    questionText = sellerInfo.categoryArchQuestion;
                    break;
                }
            case 4:
                {
                    questionText = sellerInfo.categorySocialQuestion;
                    break;
                }
        }
        uiManager.SetupQuestionText(questionText);
    }

    public void SetupAnswer()
    {
        string answerText = "";

        switch (questionSetCounter)
        {
            case 0:
                {
                    answerText = buyerManager.currentActiveBuyer.categorySpaceAnswer;
                    break;
                }
            case 1:
                {
                    answerText = buyerManager.currentActiveBuyer.categoryConvAnswer;
                    break;
                }
            case 2:
                {
                    answerText = buyerManager.currentActiveBuyer.categoryFeatAnswer;
                    break;
                }
            case 3:
                {
                    answerText = buyerManager.currentActiveBuyer.categoryArchAnswer;
                    break;
                }
            case 4:
                {
                    answerText = buyerManager.currentActiveBuyer.categorySocialAnswer;
                    break;
                }
        }
        uiManager.SetupAnswerText(answerText);
    }

    public void SetupNextQuestion()
    {
        questionSetCounter++;
        if (questionSetCounter >= 5)
        {
            questionSetCounter = 0;
        }
        AudioManager.Instance.PlayThisClip(AUDIO_SFX.BUTTON_POP);
        SetupQuestion();
        SetupAnswer();
    }

    public void SetupPreviousQuestion()
    {
        questionSetCounter--;
        if (questionSetCounter < 0)
        {
            questionSetCounter = 4;
        }
        AudioManager.Instance.PlayThisClip(AUDIO_SFX.BUTTON_POP);
        SetupQuestion();
        SetupAnswer();
    }

    public void SetupNewBuyer()
    {
        int questionAnswerIndexSpace = Random.Range(0, categoriesData.categorySpaceInfo.questionOptions.Count);
        int questionAnswerIndexConv = Random.Range(0, categoriesData.categoryConvInfo.questionOptions.Count);
        int questionAnswerIndexFeat = Random.Range(0, categoriesData.categoryFeatInfo.questionOptions.Count);
        int questionAnswerIndexArch = Random.Range(0, categoriesData.categoryArchInfo.questionOptions.Count);
        int questionAnswerIndexSocial = Random.Range(0, categoriesData.categorySocialInfo.questionOptions.Count);

        sellerInfo.SetupQuestionSet(questionAnswerIndexSpace, questionAnswerIndexConv, questionAnswerIndexFeat, questionAnswerIndexArch, questionAnswerIndexSocial);
        buyerManager.SpawnBuyer(questionAnswerIndexSpace, questionAnswerIndexConv, questionAnswerIndexFeat, questionAnswerIndexArch, questionAnswerIndexSocial);

        SetupAnswer();
        SetupQuestion();
    }

    public bool CompareSubmittedRoom(RoomInfo roomInfo)
    {
        bool buyerLeft = true;

        float maxValues = 2;

        float enumValueDifference = (Mathf.Abs((int)roomInfo.categorySpace - (int)buyerManager.currentActiveBuyer.categorySpace));

        playerScore = 0;

        if (enumValueDifference == 0)
        {
            playerScore += maxCommision;
        }

        enumValueDifference = (Mathf.Abs((int)roomInfo.categorySocial - (int)buyerManager.currentActiveBuyer.categorySocial));

        if (enumValueDifference == 0)
        {
            playerScore += maxCommision;
        }

        enumValueDifference = (Mathf.Abs((int)roomInfo.categoryFeatures - (int)buyerManager.currentActiveBuyer.categoryFeatures));

        if (enumValueDifference == 0)
        {
            playerScore += maxCommision;
        }

        enumValueDifference = (Mathf.Abs((int)roomInfo.categoryConv - (int)buyerManager.currentActiveBuyer.categoryConv));

        if (enumValueDifference == 0)
        {
            playerScore += maxCommision;
        }

        enumValueDifference = (Mathf.Abs((int)roomInfo.categoryArch - (int)buyerManager.currentActiveBuyer.categoryArch));

        if (enumValueDifference
            == 0)
        {
            playerScore += maxCommision;
        }

        playerTotalScore += playerScore;

        houseSold++;
        if (houseSold < 8)
        {
            SetupNewBuyer();
        }
        else
        {
            buyerLeft = false;
        }

        return buyerLeft;
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

}
