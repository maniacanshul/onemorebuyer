﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SellerInfo : MonoBehaviour
{
    public string categorySpaceQuestion;
    public string categoryConvQuestion;
    public string categoryFeatQuestion;
    public string categoryArchQuestion;
    public string categorySocialQuestion;

    private GameManager gameManager;

    public void Init(GameManager _gameManager)
    {
        gameManager = _gameManager;
    }

    public void SetupQuestionSet(int space, int conv, int feat, int arch, int social)
    {
        categorySpaceQuestion = gameManager.categoriesData.categorySpaceInfo.questionOptions[space];

        categoryConvQuestion = gameManager.categoriesData.categoryConvInfo.questionOptions[conv];

        categoryFeatQuestion = gameManager.categoriesData.categoryFeatInfo.questionOptions[feat];

        categoryArchQuestion = gameManager.categoriesData.categoryArchInfo.questionOptions[arch];

        categorySocialQuestion = gameManager.categoriesData.categorySocialInfo.questionOptions[social];
    }
}
