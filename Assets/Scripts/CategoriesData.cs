﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CategoriesData : MonoBehaviour
{
    public CategorySpaceInfo categorySpaceInfo;
    public CategoryConvInfo categoryConvInfo;
    public CategoryFeatInfo categoryFeatInfo;
    public CategoryArchInfo categoryArchInfo;
    public CategorySocialInfo categorySocialInfo;

    public string GetCategorySpaceAnswer(CATEGORY_SPACE categorySpace)
    {
        string tempAnswer = "";

        for (int i = 0; i < categorySpaceInfo.answersInfo.Count; i++)
        {
            if (categorySpace == categorySpaceInfo.answersInfo[i].categorySpace)
            {
                tempAnswer = categorySpaceInfo.answersInfo[i].answerOptions[Random.Range(0, categorySpaceInfo.answersInfo[i].answerOptions.Count)];
                break;
            }
        }
        return tempAnswer;
    }

    public string GetCategoryConvAnswer(CATEGORY_CONV categoryConv)
    {
        string tempAnswer = "";

        for (int i = 0; i < categoryConvInfo.answersInfo.Count; i++)
        {
            if (categoryConv == categoryConvInfo.answersInfo[i].categoryConv)
            {
                int randomAnswer = Random.Range(0, categoryConvInfo.answersInfo[i].answerOptions.Count);
                tempAnswer = categoryConvInfo.answersInfo[i].answerOptions[randomAnswer];
                break;
            }
        }
        return tempAnswer;
    }


    public string GetCategoryFeatAnswers(CATEGORY_FEATURES categoryFeat)
    {
        string tempAnswer = "";

        for (int i = 0; i < categoryFeatInfo.answersInfo.Count; i++)
        {
            if (categoryFeat == categoryFeatInfo.answersInfo[i].categoryFeat)
            {
                int randomAnswer = Random.Range(0, categoryFeatInfo.answersInfo[i].answerOptions.Count);
                tempAnswer = categoryFeatInfo.answersInfo[i].answerOptions[randomAnswer];
                break;
            }
        }

        return tempAnswer;
    }

    public string GetCategoryArchAnswers(CATEGORY_ARCH categoryArch)
    {
        string tempAnswer = "";

        for (int i = 0; i < categoryArchInfo.answersInfo.Count; i++)
        {
            if (categoryArch == categoryArchInfo.answersInfo[i].categoryArch)
            {
                int randomAnswer = Random.Range(0, categoryArchInfo.answersInfo[i].answerOptions.Count);
                tempAnswer = categoryArchInfo.answersInfo[i].answerOptions[randomAnswer];
                break;
            }
        }

        return tempAnswer;
    }


    public string GetCategorySocialAnswers(CATEGORY_SOCIAL categorySocial)
    {
        string tempAnswer = "";

        for (int i = 0; i < categorySocialInfo.answersInfo.Count; i++)
        {
            if (categorySocial == categorySocialInfo.answersInfo[i].categorySocial)
            {
                int randomAnswer = Random.Range(0, categorySocialInfo.answersInfo[i].answerOptions.Count);
                tempAnswer = categorySocialInfo.answersInfo[i].answerOptions[randomAnswer];
                break;
            }
        }

        return tempAnswer;
    }

    public string GetRoomConvDesc(CATEGORY_CONV categoryConv)
    {
        string tempDesc = "";

        for (int i = 0; i < categoryConvInfo.answersInfo.Count; i++)
        {
            if (categoryConv == categoryConvInfo.answersInfo[i].categoryConv)
            {
                int randomAnswer = Random.Range(0, categoryConvInfo.answersInfo[i].houseDesc.Count);
                tempDesc = categoryConvInfo.answersInfo[i].houseDesc[randomAnswer];
                break;
            }
        }

        return tempDesc;
    }

    public string GetRoomSpaceDesc(CATEGORY_SPACE categorySpace)
    {
        string tempDesc = "";

        for (int i = 0; i < categorySpaceInfo.answersInfo.Count; i++)
        {
            if (categorySpace == categorySpaceInfo.answersInfo[i].categorySpace)
            {
                int randomAnswer = Random.Range(0, categorySpaceInfo.answersInfo[i].houseDesc.Count);
                tempDesc = categorySpaceInfo.answersInfo[i].houseDesc[randomAnswer];
                break;
            }
        }

        return tempDesc;
    }

    public string GetRoomFeatDesc(CATEGORY_FEATURES categoryFeat)
    {
        string tempDesc = "";

        for (int i = 0; i < categoryFeatInfo.answersInfo.Count; i++)
        {
            if (categoryFeat == categoryFeatInfo.answersInfo[i].categoryFeat)
            {
                int randomAnswer = Random.Range(0, categoryFeatInfo.answersInfo[i].houseDesc.Count);
                tempDesc = categoryFeatInfo.answersInfo[i].houseDesc[randomAnswer];
                break;
            }
        }

        return tempDesc;
    }

    public string GetRoomArchDesc(CATEGORY_ARCH categoryArch)
    {
        string tempDesc = "";

        for (int i = 0; i < categoryArchInfo.answersInfo.Count; i++)
        {
            if (categoryArch == categoryArchInfo.answersInfo[i].categoryArch)
            {
                int randomAnswer = Random.Range(0, categoryArchInfo.answersInfo[i].houseDesc.Count);
                tempDesc = categoryArchInfo.answersInfo[i].houseDesc[randomAnswer];
                break;
            }
        }

        return tempDesc;
    }

    public string GetRoomSocialDesc(CATEGORY_SOCIAL categorySocial)
    {
        string tempDesc = "";

        for (int i = 0; i < categorySocialInfo.answersInfo.Count; i++)
        {
            if (categorySocial == categorySocialInfo.answersInfo[i].categorySocial)
            {
                int randomAnswer = Random.Range(0, categorySocialInfo.answersInfo[i].houseDesc.Count);
                tempDesc = categorySocialInfo.answersInfo[i].houseDesc[randomAnswer];
                break;
            }
        }

        return tempDesc;
    }
}

//Space info
[System.Serializable]
public class CategorySpaceInfo
{
    public List<CategorySpaceAnswersInfo> answersInfo;
    public List<string> questionOptions;
}

//Space answers info
[System.Serializable]
public class CategorySpaceAnswersInfo
{
    public CATEGORY_SPACE categorySpace;
    public List<string> answerOptions;
    public List<string> houseDesc;
}

//Conv info
[System.Serializable]
public class CategoryConvInfo
{
    public List<CategoryConvAnswersInfo> answersInfo;
    public List<string> questionOptions;
}

//Conv answers info
[System.Serializable]
public class CategoryConvAnswersInfo
{
    public CATEGORY_CONV categoryConv;
    public List<string> answerOptions;
    public List<string> houseDesc;
}

//Features info
[System.Serializable]
public class CategoryFeatInfo
{
    public List<CategoryFeatAnswersInfo> answersInfo;
    public List<string> questionOptions;
}

//Features answers info
[System.Serializable]
public class CategoryFeatAnswersInfo
{
    public CATEGORY_FEATURES categoryFeat;
    public List<string> answerOptions;
    public List<string> houseDesc;
}

//Arch info
[System.Serializable]
public class CategoryArchInfo
{
    public List<CategoryArchAnswersInfo> answersInfo;
    public List<string> questionOptions;
}

//Arch answers info
[System.Serializable]
public class CategoryArchAnswersInfo
{
    public CATEGORY_ARCH categoryArch;
    public List<string> answerOptions;
    public List<string> houseDesc;
}

//Social info
[System.Serializable]
public class CategorySocialInfo
{
    public List<CategorySocialAnswersInfo> answersInfo;
    public List<string> questionOptions;
}

//Social answers info
[System.Serializable]
public class CategorySocialAnswersInfo
{
    public CATEGORY_SOCIAL categorySocial;
    public List<string> answerOptions;
    public List<string> houseDesc;
}



