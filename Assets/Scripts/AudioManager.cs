﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource audioSource;

    public static AudioManager Instance { get; private set; }
    void Awake()
    {
        if (Instance == null) { Instance = this; } else { Debug.Log("Warning: multiple " + this + " in scene! This one is at " + transform.name); }
    }

    public List<GameSFX> gameSfxs;

    public void PlayThisClip(AUDIO_SFX sfx)
    {
        for (int i = 0; i < gameSfxs.Count; i++)
        {
            if (gameSfxs[i].audioSFX == sfx)
            {
                AudioClip temp = gameSfxs[i].audioClip;
                audioSource.Stop();
                if (temp != null)
                {
                    audioSource.PlayOneShot(temp);
                }
                break;
            }
        }
    }

}

[System.Serializable]
public class GameSFX
{
    public AudioClip audioClip;
    public AUDIO_SFX audioSFX;
}

public enum AUDIO_SFX
{
    BUTTON_POP,
    CHAT_1,
    CHAT_2,
}
