﻿public enum CATEGORY_SPACE
{
    SPACE_SMALL,
    SPACE_BIG
}

public enum CATEGORY_CONV
{
    CONV_RESOURCES,
    CONV_NEARBY
}

public enum CATEGORY_FEATURES
{
    FEAT_GARAGE,
    FEAT_POOL
}

public enum CATEGORY_ARCH
{
    ARCH_MODERN,
    ARCH_HISTORICAL
}

public enum CATEGORY_SOCIAL
{
    EXTROVERT,
    INTROVERT
}

public enum GAME_STATE
{
    CONVERSATION,
    ROOM_SELECTION,
    ROOM_INFO   
}


