﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIManager : MonoBehaviour
{
    [Header("Conversation")]
    public GameObject conversationPanel;
    public Text questionText;
    public Text answerText;
    public GameObject questionBubble;
    public GameObject answerBubble;
    public Image buyerBody;
    public Image buyerHead;
    public GameObject buyerHeadForNodding;
    public GameObject sellerHeadForNodding;
    public List<Color> buyerColors;

    [Header("Room Selection")]
    public GameObject roomSelectionPanel;
    public List<Button> roomButtons;

    [Header("Room Info")]
    public GameObject roomInfoPanel;
    public Text categoryConvDesc;
    public Text categorySpaceDesc;
    public Text categoryFeatDesc;
    public Text categoryArchDesc;
    public Text categorySocialDesc;
    public Image roomImage;
    public Text scoreText;
    public Text toalScoreText;
    public GameObject gameOver;
    public GameObject nextBuyerButton;
    public GameObject gameOverButton;
    public GameObject backButton;
    public GameObject selectButton;
    public GameObject soldStamp;

    private GameManager gameManager;
    private CategoriesData categoriesData;

    private RoomInfo currentlySelectedRoom;

    public void Init(GameManager _gameManager)
    {
        gameManager = _gameManager;
        categoriesData = gameManager.categoriesData;
        currentlySelectedRoom = null;
        SetupButtonsSprites();
    }

    private void SetupButtonsSprites()
    {
        for (int i = 0; i < roomButtons.Count; i++)
        {
            roomButtons[i].GetComponent<Image>().sprite = gameManager.roomManager.roomSpawned[i].roomSprites[i];
            roomButtons[i].transform.GetChild(0).GetComponent<Image>().sprite = gameManager.roomManager.roomSpawned[i].roomSpritesHighlighted[i];
        }
    }

    public void SetupQuestionText(string text)
    {
        questionText.gameObject.SetActive(false);
        questionBubble.SetActive(false);
        questionText.text = text;
        Invoke("UpdateQuestionTextAfterDelay", 0.5f);
    }

    public void SetupAnswerText(string text)
    {
        answerText.gameObject.SetActive(false);
        answerBubble.SetActive(false);
        answerText.text = text;
        Invoke("UpdateAnswerTextAfterDelay", 2f);
    }

    private void UpdateQuestionTextAfterDelay()
    {
        questionText.gameObject.SetActive(true);
        questionBubble.SetActive(true);
        AudioManager.Instance.PlayThisClip(AUDIO_SFX.CHAT_2);
        sellerHeadForNodding.transform.DOPunchRotation(new Vector3(0, 0, 1f), 0.3f).SetEase(Ease.InOutElastic).OnComplete( () => 
        {
            sellerHeadForNodding.transform.DOPunchRotation(new Vector3(0, 0, 0.3f), 0.3f).SetEase(Ease.InOutElastic);
        });
    }

    private void UpdateAnswerTextAfterDelay()
    {
        answerText.gameObject.SetActive(true);
        answerBubble.SetActive(true);
        AudioManager.Instance.PlayThisClip(AUDIO_SFX.CHAT_1);
        buyerHead.transform.DOPunchRotation(new Vector3(0, 0, 2f), 0.3f).SetEase(Ease.InOutElastic).OnComplete(() =>
        {
            buyerHead.transform.DOPunchRotation(new Vector3(0, 0, 0.6f), 0.3f).SetEase(Ease.InOutElastic);
        });
    }

    public void UpdateGameState(int gameStateEnumValue)
    {
        gameManager.gameState = (GAME_STATE)gameStateEnumValue;

        switch (gameManager.gameState)
        {
            case GAME_STATE.CONVERSATION:
                {
                    int randomColor = Random.Range(0, buyerColors.Count);
                    buyerBody.color = buyerColors[randomColor];
                    buyerHead.color = buyerColors[randomColor];

                    conversationPanel.SetActive(true);
                    roomSelectionPanel.SetActive(false);
                    roomInfoPanel.SetActive(false);
                    break;
                }
            case GAME_STATE.ROOM_SELECTION:
                {
                    conversationPanel.SetActive(false);
                    roomSelectionPanel.SetActive(true);
                    roomInfoPanel.SetActive(false);
                    break;
                }
            case GAME_STATE.ROOM_INFO:
                {
                    nextBuyerButton.SetActive(false);
                    soldStamp.SetActive(false);
                    gameOver.SetActive(false);
                    toalScoreText.gameObject.SetActive(false);
                    scoreText.gameObject.SetActive(false);
                    gameOverButton.SetActive(false);
                    backButton.SetActive(true);
                    selectButton.SetActive(true);
                    conversationPanel.SetActive(false);
                    roomSelectionPanel.SetActive(false);
                    roomInfoPanel.SetActive(true);
                    break;
                }
        }
    }

    public void SetupRoomInfo(int index)
    {
        currentlySelectedRoom = gameManager.roomManager.GetRoomInfo(index);

        categoryConvDesc.text = categoriesData.GetRoomConvDesc(currentlySelectedRoom.categoryConv);

        categorySpaceDesc.text = categoriesData.GetRoomSpaceDesc(currentlySelectedRoom.categorySpace);

        categoryFeatDesc.text = categoriesData.GetRoomFeatDesc(currentlySelectedRoom.categoryFeatures);

        categoryArchDesc.text = categoriesData.GetRoomArchDesc(currentlySelectedRoom.categoryArch);

        categorySocialDesc.text = categoriesData.GetRoomSocialDesc(currentlySelectedRoom.categorySocial);

        AudioManager.Instance.PlayThisClip(AUDIO_SFX.BUTTON_POP);


        roomImage.sprite = roomButtons[index].GetComponent<Image>().sprite;

        for (int i = 0; i < roomButtons.Count; i++)
        {
            roomButtons[i].transform.GetChild(0).gameObject.SetActive(false);
        }

    }

    public void SumbitRoom()
    {
        bool buyerLeft = gameManager.CompareSubmittedRoom(currentlySelectedRoom);

        PlayButtonPopAudioClip();

        int index = gameManager.roomManager.roomSpawned.IndexOf(currentlySelectedRoom);
        roomButtons[index].GetComponent<Button>().interactable = false;
        roomButtons[index].transform.GetChild(1).gameObject.SetActive(true);
        roomButtons[index].transform.GetChild(0).GetComponent<Image>().enabled = (false);

        nextBuyerButton.SetActive(false);
        gameOverButton.SetActive(false);
        backButton.SetActive(false);
        selectButton.SetActive(false);
        if (buyerLeft)
        {
            nextBuyerButton.SetActive(true);
        }
        else
        {
            gameOver.SetActive(true);
            gameOverButton.SetActive(true);
        }
        soldStamp.SetActive(true);
        toalScoreText.gameObject.SetActive(true);
        toalScoreText.text = "Total commision earned $" + gameManager.playerTotalScore;
        scoreText.gameObject.SetActive(true);
        scoreText.text = "You just earned $" + gameManager.playerScore + " as commision.";
    }

    public void PlayButtonPopAudioClip()
    {
        AudioManager.Instance.PlayThisClip(AUDIO_SFX.BUTTON_POP);
    }

}
