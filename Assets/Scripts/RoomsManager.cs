﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomsManager : MonoBehaviour
{
    public GameObject room;

    private GameManager gameManager;

    [HideInInspector]
    public List<RoomInfo> roomSpawned;

    public void Init(GameManager _gameManager)
    {
        gameManager = _gameManager;
        SpawnRoom();
    }

    private void SpawnRoom()
    {
        for (int i = 0; i < 8; i++)
        {
            GameObject tempRoom = Instantiate(room) as GameObject;
            RoomInfo roomInfo = tempRoom.GetComponent<RoomInfo>();

            roomInfo.Init(this);

            roomSpawned.Add(roomInfo);
        }
    }

    public RoomInfo GetRoomInfo(int index)
    {
        return roomSpawned[index];       
    }
}
